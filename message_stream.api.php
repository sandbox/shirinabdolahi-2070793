<?php

function message_stream_insert_uid($message, $uid) {
  db_insert('message_stream')
          ->fields(array(
              'mid' => $message->mid,
              'uid' => $uid,
              'delivered' => 0,
              'created' => $message->timestamp,
          ))
          ->execute();
}

function message_stream_followers($uid) {
  $followers = array();
  $result = db_select('user_relationships')
          ->fields('user_relationships', array('requester_id'))
          ->condition('requestee_id', $uid)
          ->execute();
  foreach ($result as $record) {
    $followers[] = $record->requester_id;
  }
  return $followers;
}

function message_stream_node_author($nid) {
  $node_authore = array();
  $node = node_load($nid);
  $node_authore[] = $node->uid;
  return $node_authore;
}

function message_stream_uid_commenters($nid) {
  $commenters = array();
  $sql = "    SELECT DISTINCT uid
              FROM  `comment`
              WHERE nid = :nid
            ";
  $result = db_query($sql, array(':nid' => $nid));
//  $result = db_select('comment')
//          ->fields('comment', array('uid'))
//          ->condition('nid', $nid)
//          ->execute();
  foreach ($result as $record) {
    $commenters[] = $record->uid;
  }
  return $commenters;
}

function message_stream_notificationCount($uid) {
  // $notifs = message_stream_all_notifications($uid);
  $notifs = db_select('message_stream')
          ->fields('message_stream', array('mid'))
          ->condition('uid', $uid)
          ->condition('delivered', 0)
          ->execute();
  $count = $notifs->rowCount();
  return $count;
}

function message_stream_all_notifications($uid, $count = NULL) {
  $query = db_select('message_stream')
          ->fields('message_stream', array('mid'))
          ->condition('uid', $uid)
          ->orderBy('created', 'DESC');
  if ($count) {
    $query->range(0, $count);
  }
  $result = $query->execute();
  return $result;
}

function message_stream_all_messages($uid, $count = NULL) {
  $notifs = message_stream_all_notifications($uid, $count);
  $mids = array();
  foreach ($notifs as $notif) {
    $mids[] = $notif->mid;
  }
  $messages = message_load_multiple($mids);
  return $messages;
}

function message_stream_viewed($uid) {
  db_update('message_stream')
          ->fields(array(
              'delivered' => 1,
          ))
          ->condition('uid', $uid)
          ->execute();
  ;
}

