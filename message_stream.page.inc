<?php

/*
 * ajax callback
 */

function message_stream_notifications_callback($ajax, $user) {
  $is_ajax = $ajax === 'ajax';
  if ($is_ajax) {
    $messages = message_stream_all_messages($user->uid, 10);
    $commands = array();
    $render_message = array();
    foreach ($messages as $message) {
      $render_message[] = entity_build_content('message', $message);
    }
    $commands[] = ajax_command_html("#notification-messages", render($render_message));
    message_stream_viewed($user->uid);
    $commands[] = ajax_command_invoke("#notification-count", "addClass", array('message-notifications-open'));
    $commands[] = ajax_command_html("#notification-count", l(0, "message-stream/notifications/nojs/" . $user->uid, array('attributes' => array('class' => 'use-ajax'))));
    return array(
        '#type' => 'ajax',
        '#commands' => $commands,
    );
  } else {
    $uri = drupal_goto('message-stream');
  }
}

function message_stream_notification_page_callback() {
  global $user;
  $messages = message_stream_all_messages($user->uid);
  foreach ($messages as $message) {
    $render_message[] = entity_build_content('message', $message);
  }
  return $render_message;
}



