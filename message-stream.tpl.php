<div id="notification-box">
  <div id="notification-count">
    <?php
    global $user;
    //print $notifications;
    print l($notifications,"message-stream/notifications/nojs/".$user->uid, array('attributes' => array('class' => 'use-ajax')));  
    ?>
  </div>
  <div id="notification-messages"></div>
</div>